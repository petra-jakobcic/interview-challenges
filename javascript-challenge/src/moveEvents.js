'use strict';

// Import the dependencies.
const events = require('./events');
const { parseISO, addDays } = require('date-fns');

/**
 * Finds an event by its ID.
 *
 * @param {Object} events The events grouped by day.
 * @param {number} id The event ID.
 *
 * @returns {Object} An object containing the event itself and
 * 					 the slot at which the event is found.
 */
const findEventById = (events, id) => {
  const eventObject = {};

  for (let key in events) {
    const arrayOfEventObjects = events[key];

    const event = arrayOfEventObjects.find((event) => {
      return event.id === id;
    });

    if (event) {
      eventObject.event = event;
      eventObject.slot = parseInt(key);
      return eventObject;
    }
  }

  // If the event does not exist,
  // throw an error.
  if (eventObject === undefined) {
    throw new Error('This event does not exist.');
  }
};

/**
 * Calculates the numerical difference between two slots.
 *
 * @param {number} currentSlot The current slot.
 * @param {number} moveToSlot The slot that the event is going to be
 * 							  moved to.
 *
 * @returns {number} The numerical difference between the two slots.
 */
const calculateDifferenceInDays = (currentSlot, moveToSlot) => {
  return moveToSlot - currentSlot;
};

/**
 * Checks whether a slot exists.
 *
 * @param {Object} events The events.
 * @param {number} slot The day difference from the earliest event.
 *
 * @returns {boolean} True if a slot exists,
 * 					  otherwise false.
 */
const slotExists = (events, slot) => {
  return slot in events;
};

/**
 * Removes an event from the given slot.
 *
 * @param {Object} groups The events grouped by their day difference.
 * @param {number} slot The day difference from the first event day.
 * @param {Object} event An event object.
 *
 * @returns {void}
 */
const removeFromSlot = (groups, slot, event) => {
  const index = groups[slot].indexOf(event);

  groups[slot].splice(index, 1);
};

/**
 * Deletes the specified slot from a group of events.
 *
 * @param {Object} events The events grouped by day.
 * @param {number} slot The slot to be deleted.
 *
 * @returns {Object} The events without the slot.
 */
const deleteSlot = (events, slot) => {
  delete events[slot];

  return events;
};

/**
 * Moves an event to a specified day.
 *
 * @param {Object} eventsByDay The events grouped by day.
 * @param {number} id The event id.
 * @param {number} toDay The day which the event is going to be moved to.
 *
 * @returns {Object} The events grouped by day containing the updated event.
 */
const moveEventToDay = (eventsByDay, id, toDay) => {
  // If the 'eventsByDay' is empty,
  // throw an error.
  if (Object.keys(eventsByDay).length === 0) {
    throw new Error('There are no events.');
  }

  const sortedEvents = {
    ...eventsByDay,
  };

  // Grab the event to be moved.
  const eventToBeMoved = findEventById(sortedEvents, id);

  // Calculate the difference between the current slot and toDay slot.
  const differenceInDays = calculateDifferenceInDays(
    eventToBeMoved.slot,
    toDay,
  );

  // Get the current start & end date of the event.
  const currentStartDate = parseISO(eventToBeMoved.event.startsAt);
  const currentEndDate = parseISO(eventToBeMoved.event.endsAt);

  // Calculate the new start & end date of the event.
  const newStartDate = addDays(currentStartDate, differenceInDays);
  const newEndDate = addDays(currentEndDate, differenceInDays);

  // Set the new start & end date of the event.
  eventToBeMoved.event.startsAt = newStartDate;
  eventToBeMoved.event.endsAt = newEndDate;

  // If the slot exists:
  // -delete the event from the previous slot
  // -push the event into the array at that slot
  // -sort the array
  // -set the new slot for the event
  if (slotExists(sortedEvents, toDay)) {
    removeFromSlot(sortedEvents, eventToBeMoved.slot, eventToBeMoved.event);
    events.addToSlot(sortedEvents, toDay, eventToBeMoved.event);
    events.sortEvents(sortedEvents[toDay]);
    eventToBeMoved.slot = toDay;
  }

  // If the slot does NOT exist:
  // -delete the event from the previous slot
  // -push the event into the array at the new slot
  // -set the new slot for the event
  if (!slotExists(sortedEvents, toDay)) {
    removeFromSlot(sortedEvents, eventToBeMoved.slot, eventToBeMoved.event);

    if (eventToBeMoved.slot !== 0) {
      deleteSlot(sortedEvents, eventToBeMoved.slot);
    }

    sortedEvents[toDay] = [];
    sortedEvents[toDay].push(eventToBeMoved.event);
    eventToBeMoved.slot = toDay;
  }

  return sortedEvents;
};

module.exports = {
  findEventById,
  calculateDifferenceInDays,
  slotExists,
  removeFromSlot,
  deleteSlot,
  moveEventToDay,
};
