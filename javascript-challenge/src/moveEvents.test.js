// Import our dependencies.
const groupEvents = require('./events');
const moveEvents = require('./moveEvents');
const testData = require('./testData');

// Write the tests.
describe('The "findEventById" function', () => {
  test('finds an event by its ID', () => {
    // Arrange
    const events = groupEvents.groupEventsByDayDifference(testData.myEvents);
    const id = 111;
    const expected = {
      event: testData.cook,
      slot: 59,
    };

    // Act
    const result = moveEvents.findEventById(events, id);

    // Assert
    expect(result).toEqual(expected);
  });

  test('finds an event by its ID', () => {
    // Arrange
    const events = groupEvents.groupEventsByDayDifference(testData.myEvents);
    const id = 109;
    const expected = {
      event: testData.playGuitar,
      slot: 0,
    };

    // Act
    const result = moveEvents.findEventById(events, id);

    // Assert
    expect(result).toEqual(expected);
  });

  // test('throws an error if an event does not exist', () => {
  //     // Arrange
  //     const events = groupEvents.groupEventsByDayDifference(testData.myEvents);
  //     const id = 123;
  //     const errorMessage = "This event does not exist.";

  //     // Act
  //     const result = moveEvents.findEventById(events, id);

  //     // Assert
  //     expect(() => result).toThrowError(errorMessage);
  // });
});

describe('The "calculateDifferenceInDays" function', () => {
  test('calculates the difference between two slots', () => {
    // Arrange
    const currentSlot = 3;
    const newSlot = 10;
    const expected = 7;

    // Act
    const result = moveEvents.calculateDifferenceInDays(currentSlot, newSlot);

    // Assert
    expect(result).toBe(expected);
  });

  test('calculates the difference between two slots', () => {
    // Arrange
    const currentSlot = 10;
    const newSlot = 4;
    const expected = -6;

    // Act
    const result = moveEvents.calculateDifferenceInDays(currentSlot, newSlot);

    // Assert
    expect(result).toBe(expected);
  });
});

describe('The "slotExists" function', () => {
  test('returns true if a slot exists', () => {
    // Arrange
    const eventsByDay = groupEvents.groupEventsByDayDifference(
      testData.myEvents,
    );
    const slot = 0;

    // Act
    const result = moveEvents.slotExists(eventsByDay, slot);

    // Assert
    expect(result).toBe(true);
  });

  test('returns false if a slot does not exist', () => {
    // Arrange
    const events = testData.myEvents;
    const slot = 13;

    // Act
    const result = moveEvents.slotExists(events, slot);

    // Assert
    expect(result).toBe(false);
  });
});

describe('The "removeFromSlot" function', () => {
  test('removes an event from the given slot', () => {
    // Arrange
    const eventsByDay = groupEvents.groupEventsByDayDifference(
      testData.myEvents,
    );
    const slot = 59;
    const expected = {
      0: [testData.playGuitar],
      59: [testData.walkOscar, testData.cook],
      75: [testData.practiceJavaScript],
    };

    // Act
    moveEvents.removeFromSlot(eventsByDay, slot, testData.goCycling);
    const result = eventsByDay;

    // Assert
    expect(result).toEqual(expected);
  });
});

describe('The "deleteSlot" function', () => {
  test('deletes the slot if the array with events is empty', () => {
    // Arrange
    const events = {
      4: [testData.walkOscar, testData.goCycling],
      13: [],
    };

    const slot = 13;
    const expected = { 4: [testData.walkOscar, testData.goCycling] };

    // Act
    const result = moveEvents.deleteSlot(events, slot);

    // Assert
    expect(result).toEqual(expected);
  });
});

describe('The "moveEventToDay" function', () => {
  test('moves an event to an existing slot', () => {
    // Arrange
    const events = {
      0: [testData.playGuitar],
      59: [testData.walkOscar, testData.cook],
      75: [testData.practiceJavaScript],
    };

    const expected = {
      0: [testData.playGuitar],
      59: [testData.walkOscar],
      75: [testData.practiceJavaScript, testData.cook],
    };

    // Act
    const result = moveEvents.moveEventToDay(events, 111, 75);

    // Assert
    expect(result).toEqual(expected);
  });

  test('moves an event to a new slot', () => {
    // Arrange
    const events = {
      0: [testData.playGuitar],
      59: [testData.walkOscar],
      75: [testData.practiceJavaScript],
    };

    const expected = {
      0: [testData.playGuitar],
      63: [testData.walkOscar],
      75: [testData.practiceJavaScript],
    };

    // Act
    const result = moveEvents.moveEventToDay(events, 107, 63);

    // Assert
    expect(result).toEqual(expected);
  });

  // test('throws an error if the array of events is empty', () => {
  //     // Arrange
  //     const events = {};
  //     const errorMessage = 'There are no events.';

  //     // Act
  //     const result = moveEvents.moveEventToDay(events, 123, 8);

  //     // Assert
  //     expect(() => result).toThrowError(errorMessage);
  // });
});
