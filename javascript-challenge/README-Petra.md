# Javascript Challenge by Petra :blush:

The functions were created using the 'date-fns' library and tested with Jest.

## Installation

1. Clone the project:
   ```
   git clone https://bitbucket.org/petra-jakobcic/interview-challenges.git
   ```
1. From the JavaScript project folder, run `npm install`
1. Initialise the tests with `npm test`
1. Enjoy! 😄

## Technologies used

- 'date-fns' JS library