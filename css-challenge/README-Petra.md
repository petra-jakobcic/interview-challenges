# CSS Challenge by Petra :sunglasses:

The design concept was implemented using a mobile-first approach.

## Installation

1. Clone the project:
   ```
   git clone https://bitbucket.org/petra-jakobcic/interview-challenges.git
   ```
1. From the CSS project folder, run `npm install`
1. Start the server with `npm start`
1. Enjoy! 😄

## Technologies used

- React
- Sass
